/*
 * Sample program for DokodeModem
 * RTC Wakeup
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>
DOKODEMO Dm = DOKODEMO();

void disp_rtc()
{
  DateTime tm = Dm.rtcNow();
  char buf[64];
  snprintf(buf, sizeof(buf), "%d-%02d-%02d %02d:%02d:%02d ", tm.year(), tm.month(), tm.day(), tm.hour(), tm.minute(), tm.second());
  UartExternal.print(buf);
}

void setup()
{
  // RTC割り込み起動でも、通常の電源ONと同じようにBootloaderから初期化されて起動されます。
  // DIP-SWの3をONにすると通常起動、OFFでRTC割り込み起動になりますが、
  // 最初にONにしておいてRTCを設定してから、OFFにして電源を入れなおしてください。

  // USB電源ではRTC割り込みは起動しません。DIP-SWの4をOFFにして外部電源で起動してください。

  // このプログラムを書き込むときはATMEL-ICEを使うか、DIP-SWの4をONにしてUSB電源で起動してから書き込んでください。
  // USBを認識する前に電源を切ってしまう場合、USB-UARTが使えなくなります。
  Dm.begin();

  // USB-UARTが使えない場合、デバッグ文字を出す場合は外部UARTを使ってください。
  Dm.exPowerCtrl(ON);    // 外部電源をONにしてから、
  Dm.exComPowerCtrl(ON); // 外部シリアルをONにします
  UartExternal.begin(115200);

  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, OFF);

  // 起動要因を見ます
  if (digitalRead(POW_STA_IN) == 1)
  {
    // 保存しておいた値を読みだし、更新します。
    uint32_t rtcRam = Dm.readRtcRam();
    rtcRam += 1;
    Dm.writeRtcRam(rtcRam);

    uint8_t e2data;
    Dm.e2prom_read(0, &e2data);
    e2data += 1;
    Dm.e2prom_write(0, e2data);

    // RTC 割り込みで起動したことを示します
    UartExternal.println("RTC wakeup " + String(rtcRam) + ":" + String(e2data));

    // 特に意味ないですが、経過時間を表示します
    disp_rtc();

    // 特に意味ないですが、わかりやすくLEDをちかちかさせます。
    for (int i = 0; i < 30; i++)
    {
      Dm.LedCtrl(RED_LED, ON);
      delay(100);
      Dm.LedCtrl(RED_LED, OFF);
      delay(100);
    }

    pinMode(MAIN_PWR_CNT_OUT, OUTPUT); // ここで電源が切れます
  }
  else
  {
    // 通常電源入りで起動しました
    UartExternal.println("Normal power on");

    // 特に必要ないですが、RTCを初期化設定します
    Dm.rtcAdjust(DateTime(2024, 1, 1, 0, 0, 0));

    // RTC割り込み周期を設定します
    Dm.setRtcTimer(RtcTimerPeriod::SECONDS, 10);

    // 必要であれば、RTCのRAMに4バイト保存できます。
    Dm.writeRtcRam(0);

    // 必要であればE2PROMに保存しても良いと思います。
    // 書き込んだ値が保存できているかは読みだして確認してください。
    Dm.e2prom_write(0, 0);

    // 特に意味ないですが、時間を表示します
    disp_rtc();
  }
}

void loop()
{
  // ここは通常電源入りの時だけ動きます
  Dm.LedCtrl(RED_LED, ON);
  Dm.LedCtrl(GREEN_LED, ON);
  delay(500);
  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, OFF);
  delay(500);
}
